------------------------------------------
--	iEnsomatic RealisticVehicleFailure  --
------------------------------------------
--
--	Created by Jens Sandalgaard
--
--	This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
--
--	https://github.com/iEns/RealisticVehicleFailure
--
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local function checkWhitelist(id)
	for key, value in pairs(RepairWhitelist) do
		if id == value then
			return true
		end
	end
	return false
end

AddEventHandler('chatMessage', function(source, _, message)
	local msg = string.lower(message)
	local identifier = GetPlayerIdentifiers(source)[1]
	if msg == "/fix" then
		CancelEvent()
		if RepairEveryoneWhitelisted == true then
			TriggerClientEvent('esx_vehiclefailure:repair', source)
		else
			if checkWhitelist(identifier) then
				TriggerClientEvent('esx_vehiclefailure:repair', source)
			else
				TriggerClientEvent('esx_vehiclefailure:notAllowed', source)
			end
		end
	end
end)

RegisterServerEvent('esx_vehiclefailure:takemoney')
AddEventHandler('esx_vehiclefailure:takemoney', function(repairCost)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeMoney(repairCost)
end)
