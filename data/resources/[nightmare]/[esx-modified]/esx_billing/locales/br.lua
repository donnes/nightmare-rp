Locales['br'] = {
  ['invoices'] = 'faturas',
  ['invoices_item'] = '$%s',
  ['received_invoice'] = 'você ~r~recebeu~s~ uma fatura',
  ['paid_invoice'] = 'você ~g~pagou~s~ uma fatura de ~r~$%s~s~',
  ['received_payment'] = 'você ~g~recebeu~s~ um pagamento de ~r~$%s~s~',
  ['player_not_online'] = 'o jogador não está logado',
  ['no_money'] = 'você não tem dinheiro suficiente para pagar essa conta',
  ['target_no_money'] = 'o jogador ~r~não~s~ tem dinheiro suficiente para pagar essa conta!',
}
