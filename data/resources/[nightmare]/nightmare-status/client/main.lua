local show = true
local thirst = 0
local hunger = 0
local armor = 0
local talking = false

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(250)

		print(IsHudHidden())

		if IsHudHidden() == 1 or IsPauseMenuActive() === true then
			show = false
		end

		print(show)

		SendNUIMessage({
			show = show,
			thirst = thirst,
			hunger = hunger,
			armor = armor,
			talking = talking,
		})
	end
end)

AddEventHandler("nightmare-status:onTick", function(data)
	hunger = data[1].percent
	thirst = data[2].percent
end)

AddEventHandler("nightmare-status:onPlayerTalking", function(status)
	talking = status
end)
