NIGHTMARE = {}

-- Notification event
RegisterNetEvent('nightmare-base:showNotification')
AddEventHandler('nightmare-base:showNotification', function(msg, type)
	NIGHTMARE.ShowNotification(msg, type)
end)

-- Notification
NIGHTMARE.ShowNotification = function(msg, type)
	SendNUIMessage({
		toast = {
			message = msg,
			type = type
		}
	})
end

-- Draw text on screen
NIGHTMARE.DrawAdvancedText = function(x, y, sc, text, r, g, b, a, font, jus)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(sc, sc)
	N_0x4e096588b13ffeca(jus)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x, y)
end
