ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

--------------------------
---- Server Callbacks ----
--------------------------

-----------------------
--- Local Functions ---
-----------------------

-- Get user wallet money
local function getMoneyFromUser(id_user)
	local xPlayer = ESX.GetPlayerFromId(id_user)
	return xPlayer.getMoney()
end

-- Get user black money
local function getBlackMoneyFromUser(id_user)
	local xPlayer = ESX.GetPlayerFromId(id_user)
	local account = xPlayer.getAccount('black_money')
	return account.money
end

-- Get user bank money
local function getBankFromUser(id_user)
	local xPlayer = ESX.GetPlayerFromId(id_user)
	local account = xPlayer.getAccount('bank')
	return account.money
end
