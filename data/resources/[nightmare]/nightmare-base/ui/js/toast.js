window.onload = function(e) {
  window.addEventListener('message', function(event) {
    if (event.data.toast) {
      const colors = {
        success: 'linear-gradient(to right, #00b09b, #96c93d)',
        error: 'linear-gradient(to right, #ff5f6d, #ffc371)',
        default: 'linear-gradient(to right, #73a5ff, #5477f5)'
      };
      const toast = event.data.toast;

      Toastify({
        text: toast.message,
        duration: 3000,
        close: false,
        gravity: 'top',
        positionLeft: true,
        backgroundColor: toast.type ? colors[toast.type] : colors['default']
      }).showToast();
    }
  });
};
