resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Nightmare - Base'

version '1.0.0'

client_scripts {
	'@es_extended/locale.lua',
	'locales/br.lua',
	'common/variables.lua',
	'config.lua',
	'client/common.lua',
	'client/functions.lua',
	'client/main.lua'
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'locales/br.lua',
	'common/variables.lua',
	'config.lua',
	'server/common.lua',
	'server/main.lua'
}

files {
	"ui/js/libs/toastify-js.js",
	"ui/css/libs/toastify.min.css",
	"ui/js/toast.js",
	"ui/js/main.js",
	"ui/css/styles.css",
	"ui/index.html",
}

ui_page "ui/index.html"

exports {
	'getSharedObject'
}

server_exports {
	'getSharedObject'
}

dependencies {
	'es_extended',
}
