ESX = nil
NIGHTMARE = nil
local HasAlreadyEnteredMarker = false
local LastZone = nil
local CurrentAction = nil
local CurrentActionMsg = ''
local CurrentActionData = {}

Citizen.CreateThread(function()
	SetNuiFocus(false)

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end

	ESX.TriggerServerCallback('nightmare-market:requestDBItems', function(ShopItems)
		for k,v in pairs(ShopItems) do
			Config.Zones[k].Items = v
		end
	end)
end)

-- Open and get shop items
function OpenShop(zone)
	PlayerData = ESX.GetPlayerData()
	local playerId = PlayerId()
	local playerName = GetPlayerName(playerId)
	local shopItems = {}

	for i=1, #Config.Zones[zone].Items, 1 do
		local item = Config.Zones[zone].Items[i]

		if item.limit == -1 then
			item.limit = 100
		end

		shopItems[i] = {
			label = item.label,
			item = item.item,
			price = item.price,
			limit = item.limit,
			count = 1,
			loc = zone
		}
	end

	ESX.SetTimeout(200, function()
		SendNUIMessage({
			action = 'open',
			zone = zone,
			items = shopItems,
			wallet = PlayerData.money,
			playerName = playerName,
		})
		SetNuiFocus(true, true)
	end)
end

-- Buy item
RegisterNUICallback('purchase', function(data)
	TriggerServerEvent('nightmare-market:buyItem', data.item, data.count, data.loc)
end)

-- Close market
RegisterNUICallback('quit', function()
	SetNuiFocus(false)
end)

AddEventHandler('nightmare-market:hasEnteredMarker', function(zone)
	CurrentAction = 'open'
	CurrentActionMsg = _U('press_menu')
	CurrentActionData = {zone = zone}
end)

AddEventHandler('nightmare-market:hasExitedMarker', function(zone)
	CurrentAction = nil
	SetNuiFocus(false)
end)

-- Create Blips
Citizen.CreateThread(function()
	for k,v in pairs(Config.Zones) do
		for i = 1, #v.Pos, 1 do
			local blip = AddBlipForCoord(v.Pos[i].x, v.Pos[i].y, v.Pos[i].z)
			SetBlipSprite (blip, 52)
			SetBlipDisplay(blip, 4)
			SetBlipScale  (blip, 1.0)
			SetBlipColour (blip, 2)
			SetBlipAsShortRange(blip, true)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString(_U('shops'))
			EndTextCommandSetBlipName(blip)
		end
	end
end)

-- Display markers
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local coords = GetEntityCoords(PlayerPedId())

		for k,v in pairs(Config.Zones) do
			for i = 1, #v.Pos, 1 do
				if(Config.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos[i].x, v.Pos[i].y, v.Pos[i].z, true) < Config.DrawDistance) then
					DrawMarker(Config.Type, v.Pos[i].x, v.Pos[i].y, v.Pos[i].z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.Size.x, Config.Size.y, Config.Size.z, Config.Color.r, Config.Color.g, Config.Color.b, 100, false, true, 2, false, false, false, false)
				end
			end
		end
	end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			for i = 1, #v.Pos, 1 do
				if(GetDistanceBetweenCoords(coords, v.Pos[i].x, v.Pos[i].y, v.Pos[i].z, true) < Config.Size.x) then
					isInMarker  = true
					ShopItems   = v.Items
					currentZone = k
					LastZone    = k
				end
			end
		end
		if isInMarker and not HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = true
			TriggerEvent('nightmare-market:hasEnteredMarker', currentZone)
		end
		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerEvent('nightmare-market:hasExitedMarker', LastZone)
		end
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		if CurrentAction ~= nil then
			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlJustReleased(0, Keys["E"]) then
				if CurrentAction == 'open' then
					OpenShop(CurrentActionData.zone)
				end
				CurrentAction = nil
			end
		else
			Citizen.Wait(500)
		end
	end
end)
