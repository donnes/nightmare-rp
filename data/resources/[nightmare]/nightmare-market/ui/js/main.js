window.onload = function() {
  new SimpleBar(document.getElementsByClassName('nightmare-market-content')[0]);
};

Vue.use(window['vue-js-modal'].default);

Vue.component('quantity', {
  template: `
    <div class="nightmare-vue-quantity">
      <button @click="decrement()"><i class="lni-minus"></i></button>
      <input type="text" :value="quantity" readonly>
      <button @click="increment()"><i class="lni-plus"></i></button>
    </div>
  `,
  data() {
    return {
      quantity: 1
    };
  },
  props: ['max'],
  watch: {
    quantity: function(value) {
      this.$emit('input', value);
    }
  },
  methods: {
    increment() {
      if (this.quantity < this.max) {
        this.quantity++;
      }
    },
    decrement() {
      if (this.quantity > 1) {
        this.quantity--;
      }
    }
  }
});

const i18n = new VueI18n({
  locale: 'br',
  messages: window.locales
});

new Vue({
  i18n,
  el: '#app',
  data: {
    visible: false,
    name: null,
    title: 'Supermercado',
    wallet: 0,
    items: [],
    debug: null
  },
  mounted() {
    window.addEventListener('message', event => {
      const data = event.data;

      if (data.action === 'accountInfo') {
      }

      if (data.action === 'open') {
        this.visible = true;
        this.name = data.playerName;
        this.wallet = data.wallet;
        this.items = data.items;

        if (data.zone === 'RobsLiquor') {
          this.title = 'Bar';
        } else if (data.zone === 'LTDgasoline') {
          this.title = 'AM/PM';
        }
      }
    });
  },
  methods: {
    purchase: function(item) {
      fetch('http://nightmare-market/purchase', {
        method: 'POST',
        body: JSON.stringify({
          item: item.item,
          count: item.count,
          loc: item.loc
        })
      });
    },
    quit: function() {
      this.visible = false;
      fetch('http://nightmare-market/quit', {
        method: 'POST'
      });
    }
  }
});
