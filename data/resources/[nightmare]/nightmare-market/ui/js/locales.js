window.locales = {
  br: {
    welcome: 'Bem-vindo(a)',
    exit: 'Sair',
    buy: 'Comprar',
    confirm: 'Confirmar',
    cancel: 'Cancelar'
  },
  en: {
    welcome: 'Welcome',
    exit: 'Exit',
    buy: 'Buy',
    confirm: 'Confirm',
    cancel: 'Cancel'
  }
};
