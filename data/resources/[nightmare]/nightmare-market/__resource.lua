resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Nightmare - Market'

version '1.0.0'

client_scripts {
	'@es_extended/locale.lua',
	'@nightmare-base/common/variables.lua',
	'locales/br.lua',
	'config.lua',
	'client/main.lua'
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'@nightmare-base/common/variables.lua',
	'locales/br.lua',
	'config.lua',
	'server/main.lua'
}

files {
	'ui/img/icons/beer.png',
	'ui/img/icons/bread.png',
	'ui/img/icons/energy.png',
	'ui/img/icons/hamburger.png',
	'ui/img/icons/water.png',
    'ui/img/logo.png',
    'ui/css/styles.css',
    'ui/js/main.js',
	'ui/index.html',
}

ui_page "ui/index.html"

dependencies {
	'es_extended',
	'nightmare-base',
}
