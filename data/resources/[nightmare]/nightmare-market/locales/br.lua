Locales['br'] = {
	['shop'] = 'comprar',
	['shops'] = 'lojas',
	['press_menu'] = 'pressione ~INPUT_CONTEXT~ para acessar o mercado',
	['shop_item'] = '$%s',
	['bought'] = 'você acabou de comprar %sx %s por $%s',
	['not_enough'] = 'saldo insuficiente',
	['player_cannot_hold'] = 'você não tem espaço livre no seu inventário!',
	['shop_confirm'] = 'comprar %sx %s por $%s?',
	['no'] = 'não',
	['yes'] = 'sim',
  }
