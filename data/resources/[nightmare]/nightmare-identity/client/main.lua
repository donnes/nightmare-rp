ESX = nil
NIGHTMARE = nil
local isChoosing = true
local primaryCam = nil
local secondaryCam = nil

Citizen.CreateThread(function()
	SetNuiFocus(false)

	while ESX == nil do
		Citizen.Wait(200)
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end

	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)

-- Set screen view after session started and dont spawn player
Citizen.CreateThread(function()
	Citizen.Wait(7)
	if NetworkIsSessionStarted() then
		Citizen.Wait(0)
		-- TriggerServerEvent("kashactersS:SetupCharacters")
		SetScreenView()
	end
end)

-- Disable HUD and Radar while choosing/creating a character
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(7)
		if isChoosing then
			-- SetNuiFocus(true, true)
			DisplayHud(false)
			DisplayRadar(false)
		end
	end
end)

-- Set screen view and cam
function SetScreenView()
	DoScreenFadeOut(10)
	while not IsScreenFadedOut() do
		Citizen.Wait(10)
	end
	SetTimecycleModifier('hud_def_blur')
	FreezeEntityPosition(GetPlayerPed(-1), true)
	primaryCam = CreateCamWithParams("DEFAULT_SCRIPTED_CAMERA", -1355.93,-1487.78,520.75, 300.00,0.00,0.00, 100.00, false, 0)
	SetCamActive(primaryCam, true)
	RenderScriptCams(true, false, 1, true, true)
end
