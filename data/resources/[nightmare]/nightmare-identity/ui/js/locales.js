window.locales = {
  br: {
    title: 'Personagens',
    wallet: 'Carteira',
    bank: 'Banco',
    work: 'Trabalho',
    gender: 'Genero',
    date_of_birth: 'Data de nascimento',
    confirm: 'Confirmar',
    cancel: 'Cancelar',
    delete: 'Excluír',
    choose: 'Escolher',
    add_character: 'Adicionar Caracter'
  },
  en: {}
};
