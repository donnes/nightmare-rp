Vue.use(window['vue-js-modal'].default);

Vue.directive('focus', {
  inserted: function(el) {
    el.focus();
  }
});

const i18n = new VueI18n({
  locale: 'br',
  messages: window.locales
});

new Vue({
  i18n,
  el: '#app',
  data: {
    visible: true,
    characters: [],
    form: {},
    debug: null
  },
  mounted() {
    window.addEventListener('message', event => {
      const data = event.data;
    });
  },
  methods: {
    beforeCloseModal: function() {}
  }
});
