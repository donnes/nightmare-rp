Vue.use(window['vue-js-modal'].default);

Vue.directive('focus', {
  inserted: function(el) {
    el.focus();
  }
});

const i18n = new VueI18n({
  locale: 'br',
  messages: window.locales
});

new Vue({
  i18n,
  el: '#app',
  data: {
    visible: false,
    name: null,
    balance: 0,
    transactions: [],
    withdrawAmount: null,
    depositAmount: null,
    transferAmount: null,
    transferDestiny: null,
    debug: null
  },
  computed: {
    transactionColor: function(type) {
      switch (type) {
        case 'deposit':
          return '#21d520';
        case 'withdraw':
          return '#d52020';
        case 'transference':
          return '#d57520';
        default:
          return 'black';
      }
    }
  },
  mounted() {
    window.addEventListener('message', event => {
      const data = event.data;
      // this.debug = JSON.stringify(data);

      if (data.action === 'accountInfo') {
        this.visible = true;
        this.name = data.player;
        this.balance = data.balance;
      }

      if (data.action === 'logout') {
        this.visible = false;
      }
    });
  },
  methods: {
    beforeCloseModal: function() {
      this.withdrawAmount = null;
      this.depositAmount = null;
      this.transferAmount = null;
      this.transferDestiny = null;
    },
    withdraw: function() {
      fetch('http://nightmare-bank/withdraw', {
        method: 'POST',
        body: JSON.stringify({
          amount: this.withdrawAmount
        })
      });
      this.$modal.hide('withdraw-modal');
    },
    deposit: function() {
      fetch('http://nightmare-bank/deposit', {
        method: 'POST',
        body: JSON.stringify({
          amount: this.depositAmount
        })
      });
      this.$modal.hide('deposit-modal');
    },
    transfer: function() {
      fetch('http://nightmare-bank/transfer', {
        method: 'POST',
        body: JSON.stringify({
          to: this.transferDestiny,
          amount: this.transferAmount
        })
      });
      this.$modal.hide('transfer-modal');
    },
    logout: function() {
      this.visible = false;
      fetch('http://nightmare-bank/logout', {
        method: 'POST',
        body: JSON.stringify({})
      });
    }
  }
});
