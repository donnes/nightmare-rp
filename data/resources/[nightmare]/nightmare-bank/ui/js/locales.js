window.locales = {
  br: {
    welcome: 'Bem-vindo(a)',
    exit: 'Sair',
    confirm: 'Confirmar',
    cancel: 'Cancelar',
    deposit: 'Depositar',
    withdraw: 'Sacar',
    transfer: 'Transferir',
    deposit_money: 'Depositar dinheiro',
    withdraw_money: 'Sacar dinheiro',
    transfer_money: 'Transferir dinheir',
    current_balance: 'Saldo atual',
    last_transactions: 'Últimas Transações'
  },
  en: {
    welcome: 'Welcome',
    exit: 'Exit',
    confirm: 'Confirm',
    cancel: 'Cancel',
    deposit: 'Deposit',
    withdraw: 'Withdraw',
    transfer: 'Transfer',
    deposit_money: 'Deposit money',
    withdraw_money: 'Withdraw money',
    transfer_money: 'Transfer money',
    current_balance: 'Current balance',
    last_transactions: 'Last transactions'
  }
};
