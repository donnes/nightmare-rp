Locales['br'] = {
	['help'] = 'Aperte ~INPUT_PICKUP~ para acessar sua conta',
	['amount_invalid'] = 'Quantia inválida',
	['deposit_success'] = 'Depósito realizado',
	['withdraw_success'] = 'Saque realizado',
	['transfer_success'] = 'Transferencia realizado',
	['player_notfound'] = 'Usuário não encontrado',
	['self_transfer_invalid'] = 'Você não pode transferir para si mesmo',
	['insufficient_balance'] = 'Saldo insuficiente',
}
