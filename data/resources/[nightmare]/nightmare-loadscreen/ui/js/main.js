const emojis = ['😪', '💩', '😴', '😒', '😡', '🤯', '😵', '🤮', '💀'];

window.app = new Vue({
  el: '#app',
  data: {
    msg: 'Carregando 🤩',
    debug: null
  },
  mounted() {
    const index = Math.floor(Math.random() * emojis.length);
    this.msg = `Carregando ${emojis[index]}`;
    this.interval = setInterval(this.rotateEmojis, 800);

    this.listener = window.addEventListener('message', event => {
      this.init(event);
    });
  },
  methods: {
    init: function(event) {
      const self = this;
      const handlers = {
        onLogLine(data) {
          clearInterval(self.interval);
          self.msg = `${data.message} 🙏`;
        }
      };
      return (handlers[event.data.eventName] || function() {})(event.data);
    },
    rotateEmojis: function() {
      const index = Math.floor(Math.random() * emojis.length);
      this.msg = `Carregando ${emojis[index]}`;
    }
  }
});
