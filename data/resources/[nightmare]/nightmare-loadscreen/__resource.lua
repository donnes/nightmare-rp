resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Nightmare Loadscreen'

version '1.0.0'

files {
	"ui/img/logo.png",
	"ui/img/background.jpg",
	"ui/css/styles.css",
	"ui/js/main.js",
	"ui/index.html",
}

loadscreen 'ui/index.html'
