window.locales = {
  br: {
    welcome: 'Bem-vindo(a)',
    exit: 'Sair',
    confirm: 'Confirmar',
    cancel: 'Cancelar',
    use: 'Usar',
    drop: 'Dropar',
    drop_item: 'Dropar item',
    transfer: 'Transferir',
    inventory: 'Inventário'
  },
  en: {
    welcome: 'Welcome',
    exit: 'Exit',
    confirm: 'Confirm',
    cancel: 'Cancel',
    use: 'Use',
    drop: 'Drop',
    drop_item: 'Drop item',
    transfer: 'Transfer',
    inventory: 'Inventário'
  }
};
