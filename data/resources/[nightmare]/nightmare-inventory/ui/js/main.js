window.onload = function() {
  new SimpleBar(document.getElementsByClassName('nightmare-market-content')[0]);
};

Vue.use(window['vue-js-modal'].default);

Vue.directive('focus', {
  inserted: function(el) {
    el.focus();
  }
});

Vue.component('quantity', {
  template: `
    <div class="nightmare-vue-quantity">
      <button @click="decrement()"><i class="lni-minus"></i></button>
      <input type="text" :value="quantity" readonly>
      <button @click="increment()"><i class="lni-plus"></i></button>
    </div>
  `,
  data() {
    return {
      quantity: 1
    };
  },
  props: ['max'],
  watch: {
    quantity: function(value) {
      this.$emit('input', value);
    }
  },
  methods: {
    increment() {
      if (this.quantity < this.max) {
        this.quantity++;
      }
    },
    decrement() {
      if (this.quantity > 1) {
        this.quantity--;
      }
    }
  }
});

const i18n = new VueI18n({
  locale: 'br',
  messages: window.locales
});

new Vue({
  i18n,
  el: '#app',
  data: {
    visible: false,
    name: null,
    wallet: 0,
    bank: 0,
    items: [],
    dropItem: {},
    dropCount: 0,
    debug: null
  },
  mounted() {
    window.addEventListener('message', event => {
      const data = event.data;

      if (data.action === 'load') {
        const bank = data.playerData.accounts.find(obj => obj.name === 'bank');

        this.visible = true;
        this.name = data.playerName;
        this.wallet = data.playerData.money;
        this.bank = bank && bank.money;
        this.items = data.items;
      }
    });
  },
  methods: {
    beforeCloseModal: function() {
      this.dropCount = 1;
      this.dropItem = {};
    },
    useItem: function(item) {
      fetch('http://nightmare-inventory/useItem', {
        method: 'POST',
        body: JSON.stringify({
          name: item.name,
          label: item.label
        })
      });
    },
    dropItem: function() {
      fetch('http://nightmare-inventory/dropItem', {
        method: 'POST',
        body: JSON.stringify({
          name: this.dropItem.name,
          label: this.dropItem.label,
          type: this.dropItem.type,
          count: this.dropCount
        })
      }).then(() => {
        this.$modal.hide('drop-modal');
      });
    },
    quit: function() {
      this.visible = false;
      fetch('http://nightmare-inventory/quit', {
        method: 'POST'
      });
    }
  }
});
