ESX = nil
NIGHTMARE = nil
local HasAlreadyEnteredMarker = false
local LastZone = nil
local CurrentAction = nil
local CurrentActionMsg = ''
local CurrentActionData = {}

Citizen.CreateThread(function()
	SetNuiFocus(false)

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)


-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if IsControlJustReleased(0, Keys["F2"]) then
			OpenInventory()
		end
	end
end)

-- Load player inventory
function LoadPlayerInventory()
	PlayerData = ESX.GetPlayerData()
	inventory = PlayerData.inventory
	items = {}

	if inventory ~= nil then
		for key, value in pairs(inventory) do
			if inventory[key].count <= 0 then
				inventory[key] = nil
			else
				inventory[key].type = "item_standard"
				table.insert(items, inventory[key])
			end
		end
	end

	return items
end

-- Load inventory
function LoadInventory()
	PlayerData = ESX.GetPlayerData()
	local items = LoadPlayerInventory()
	local playerId = PlayerId()
	local playerName = GetPlayerName(playerId)

	SendNUIMessage({
		action = 'load',
		playerData = PlayerData,
		playerName = playerName,
		items = items
	})
end

-- Open inventory
function OpenInventory()
	LoadInventory()
	SetNuiFocus(true, true)
end

-- Use item
RegisterNUICallback('useItem', function(item)
	TriggerServerEvent("esx:useItem", item.name)
	NIGHTMARE.ShowNotification(_U('used_item', item.label), 'success')

	Citizen.Wait(250)
	LoadInventory()
end)

-- Drop item
RegisterNUICallback('dropItem', function(item)
	if IsPedSittingInAnyVehicle(playerPed) then
		NIGHTMARE.ShowNotification(_U('drop_in_vehicle', item.name), 'danger')
		return
	end

	if type(item.count) == "number" and math.floor(item.count) == item.count then
		TriggerServerEvent("esx:removeInventoryItem", item.type, item.name, item.count)
		NIGHTMARE.ShowNotification(_U('droped_item', item.label), 'success')
	end

	Citizen.Wait(250)
	LoadInventory()
end)

-- Close inventory
RegisterNUICallback('quit', function()
	SetNuiFocus(false)
end)
