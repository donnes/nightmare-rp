Locales['br'] = {
	-- atp command
	['atp_no_waypoint'] = 'Você não marcou uma localização no mapa!',
	['atp_ground'] = 'GPS não foi definido no solo! Você recebeu um paraquedas. Boa sorte!',

	-- speed limiter
	['speedlimiter_enabled'] = 'Limitador de velocidade ligado',
	['speedlimiter_disabled'] = 'Limitador de velocidade desligado',

	-- generic messages
	['message_left_reason'] = '~y~%s~s~ saiu (%s)',
	['message_left'] = '~y~%s~s~ saiu',
	['message_joined'] = '~y~%s~s~ entrou',
	['insufficient_permissions'] = 'Permissões insuficientes',

	-- no drive-by
	['nodriveby_disabled'] = 'Atirar com carro em movimento não é permitido.',

	-- commands help text
	['atp_help'] = 'Teleporte para o ponto marcado no GPS'
}
