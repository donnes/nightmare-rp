ESX = nil
NIGHTMARE = nil
local isInVehicle = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)
		isInVehicle = IsInVehicle(PlayerPedId())
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		if isInVehicle and not Config.EnableDriveby then
			SetPlayerCanDoDriveBy(PlayerId(), false)
			DisableControlAction(2, Keys['TAB'], true)
			HideHudComponentThisFrame(19) -- Weapon Wheel
			DisplayAmmoThisFrame(false)

			if IsDisabledControlJustReleased(0, Keys['TAB']) then
				NIGHTMARE.ShowNotification(_U('nodriveby_disabled'))
			end
		else
			Citizen.Wait(2000)
		end
	end
end)
