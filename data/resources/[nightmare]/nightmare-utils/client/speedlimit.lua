NIGHTMARE = nil
cruiseEnabled = false

Citizen.CreateThread(function()
	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	local resetOnEnter = true

	while true do
		Citizen.Wait(0)
		local player = GetPlayerPed(-1)
		local vehicle = GetVehicle(player)

		if IsDriver(vehicle, player) and IsDriving(player) then
			-- This should only happen on vehicle first entry to disable any old values
			if resetOnEnter then
				maxSpeed = GetVehicleMaxSpeed(vehicle)
				SetEntityMaxSpeed(vehicle, maxSpeed)
				resetOnEnter = false
			end
			-- Disable speed limiter
			if IsControlJustReleased(0, Keys["F9"]) and cruiseEnabled then
				cruiseEnabled = false
				maxSpeed = GetVehicleMaxSpeed(vehicle)
				SetEntityMaxSpeed(vehicle, maxSpeed)
				NIGHTMARE.ShowNotification(_U('speedlimiter_disabled'))
				-- Enable speed limiter
			elseif IsControlJustReleased(0, Keys["F9"]) and not cruiseEnabled then
				cruiseEnabled = true
				realSpeed = GetEntitySpeed(vehicle)

				if Config.UseMPH then
					speed = TransformToMph(realSpeed)
					if speed >= 15.53 then
						SetEntityMaxSpeed(vehicle, realSpeed)
						NIGHTMARE.ShowNotification(_U('speedlimiter_enabled'))
					end
				else
					speed = TransformToKmh(realSpeed)
					if speed >= 25 then
						SetEntityMaxSpeed(vehicle, realSpeed)
						NIGHTMARE.ShowNotification(_U('speedlimiter_enabled'))
					end
				end
			end
		else
			resetSpeedOnEnter = true
		end
	end
end)
