-- Get vehicle reference that player is inside
function GetVehicle (playerRef)
	return GetVehiclePedIsIn(playerRef, false)
end

-- Get vehicle max speed
function GetVehicleMaxSpeed (vehicleRef)
	return GetVehicleHandlingFloat(vehicleRef, "CHandlingData", "fInitialDriveMaxFlatVel")
end

-- Player is in a vehicle seat
function IsInVehicle (playerRef)
	return GetPedInVehicleSeat(GetVehicle(playerRef), -1)
end

-- Player is the vechicle driver
function IsDriver (vehicleRef, playerRef)
	return GetPedInVehicleSeat(vehicleRef, -1) == playerRef
end

-- Player is in a vehicle (not necessary as driver)
function IsDriving (playerRef)
	return IsPedInAnyVehicle(playerRef, false)
end

-- Transform vehicle speed to KM/H
function TransformToKmh (speed)
	return math.floor(speed * 3.6 + 0.5)
end

-- Transform vehicle speed to MPH
function TransformToMph (speed)
	return math.floor(speed * 2.236936 + 0.5)
end
