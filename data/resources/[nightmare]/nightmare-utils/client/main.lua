ESX = nil
NIGHTMARE = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end


	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		HideHudComponentThisFrame(3) -- SP Cash display
		HideHudComponentThisFrame(4)  -- MP Cash display
		HideHudComponentThisFrame(13) -- Cash changes
		HideHudComponentThisFrame(14) -- Hide crosshair
		DisablePlayerVehicleRewards(PlayerId()) -- No vehicle rewards. Idk what's this.
		DisableEmergencyServices() -- Disable emergency dispatch services.
	end
	SetVehicleDensityMultiplierThisFrame(0.3) -- Set vehicle traffic density
	SetPlayerHealthRechargeMultiplier(PlayerId(), 0.0) -- Disable HP auto-recovery
end)

------------------
----- Events -----
------------------

-- Admin Teleport
RegisterNetEvent('nightmare-utils:atp')
AddEventHandler('nightmare-utils:atp', function()
	local playerPed = GetPlayerPed(-1)
	local blip = GetFirstBlipInfoId(8)

	if DoesBlipExist(blip) then
		local coord = GetBlipInfoIdCoord(blip)
		local groundFound, coordZ = false, 0
		local groundCheckHeights = { 0.0, 50.0, 100.0, 150.0, 200.0, 250.0, 300.0, 350.0, 400.0,450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0 }

		for i, height in ipairs(groundCheckHeights) do

			ESX.Game.Teleport(playerPed, {
				x = coord.x,
				y = coord.y,
				z = height
			})

			local foundGround, z = GetGroundZFor_3dCoord(coord.x, coord.y, height)
			if foundGround then
				coordZ = z + 3
				groundFound = true
				break
			end
		end

		if not groundFound then
			coordZ = 100
			TriggerEvent('esx:addWeapon', 'GADGET_PARACHUTE', 0)
			NIGHTMARE.ShowNotification(_U('atp_ground'))
		end

		ESX.Game.Teleport(playerPed, {
			x = coord.x,
			y = coord.y,
			z = coordZ
		})
	else
		NIGHTMARE.ShowNotification(_U('atp_no_waypoint'))
	end
end)

-----------------
--- Functions ---
-----------------

-- Disable emergency services
function DisableEmergencyServices()
	for i = 1, 35 do
		EnableDispatchService(i, false)
	end
end
