ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

-- Admin Teleport
TriggerEvent('es:addGroupCommand', 'atp', 'admin', function(source, args, user)
	TriggerClientEvent('nightmare-utils:atp', source)
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, _U('insufficient_permissions'))
end, {
	help = _U('atp_help')
})

-- Generic Messages
if Config.EnableGenericMessages then
	AddEventHandler('esx:playerLoaded', function(source)
		TriggerClientEvent('nightmare-utils:showNotification', source, _U('message_joined', GetPlayerName(source)))
	end)

	AddEventHandler('esx:playerDropped', function(reason)
		if reason ~= "Disconnected." then
			TriggerClientEvent('nightmare-utils:showNotification', source, _U('message_left_reason', GetPlayerName(source), reason))
		else
			TriggerClientEvent('nightmare-utils:showNotification', source, _U('message_left', GetPlayerName(source)))
		end
	end)
end

-- AFK Kick
if Config.EnableAfkKick then
	RegisterServerEvent('nightmare-utils:afkkick')
	AddEventHandler('nightmare-utils:afkkick', function()
		DropPlayer(source, _U('afk_kicked_message'))
	end)
end
