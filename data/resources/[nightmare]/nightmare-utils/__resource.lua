resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Nightmare - Utils'

version '1.0.0'

client_scripts {
	'@es_extended/locale.lua',
	'@nightmare-base/common/variables.lua',
	'locales/br.lua',
	'config.lua',
	'client/functions.lua',
	'client/friendlynpc.lua',
	'client/handsup.lua',
	'client/pointfinger.lua',
	'client/npcdrop.lua',
	'client/speedlimit.lua',
	'client/afkkick.lua',
	'client/nodriveby.lua',
	'client/main.lua'
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'@nightmare-base/common/variables.lua',
	'locales/br.lua',
	'config.lua',
	'server/main.lua'
}

dependencies {
	'es_extended',
	'nightmare-base',
}
