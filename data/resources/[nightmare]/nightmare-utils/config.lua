Config = {}
Config.Locale = 'br'

-- Settings
Config.EnableGenericMessages = false
Config.EnableAfkKick = false
Config.EnableDriveby = true

-- Dev
Config.EnableDebug = false
