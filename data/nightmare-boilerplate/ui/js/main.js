window.onload = function() {
  new SimpleBar(document.getElementsByClassName('nightmare-panel-content')[0]);
};

Vue.use(window['vue-js-modal'].default);

Vue.directive('focus', {
  inserted: function(el) {
    el.focus();
  }
});

Vue.component('quantity', {
  template: `
    <div class="nightmare-vue-quantity">
      <button @click="decrement()"><i class="lni-minus"></i></button>
      <input type="text" :value="quantity" readonly>
      <button @click="increment()"><i class="lni-plus"></i></button>
    </div>
  `,
  data() {
    return {
      quantity: 1
    };
  },
  props: ['max'],
  watch: {
    quantity: function(value) {
      this.$emit('input', value);
    }
  },
  methods: {
    increment() {
      if (this.quantity < this.max) {
        this.quantity++;
      }
    },
    decrement() {
      if (this.quantity > 1) {
        this.quantity--;
      }
    }
  }
});

const i18n = new VueI18n({
  locale: 'br',
  messages: window.locales
});

new Vue({
  i18n,
  el: '#app',
  data: {
    visible: false,
    name: null,
    wallet: 0,
    bank: 0,
    debug: null
  },
  mounted() {
    window.addEventListener('message', event => {
      const data = event.data;
    });
  },
  methods: {
    beforeCloseModal: function() {},
    quit: function() {
      this.visible = false;
      fetch('http://nightmare-boilerplate/quit', {
        method: 'POST'
      });
    }
  }
});
