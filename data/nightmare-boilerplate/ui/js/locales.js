window.locales = {
  br: {
    welcome: 'Bem-vindo(a)',
    exit: 'Sair',
    confirm: 'Confirmar',
    cancel: 'Cancelar'
  },
  en: {
    welcome: 'Welcome',
    exit: 'Exit',
    confirm: 'Confirm',
    cancel: 'Cancel'
  }
};
