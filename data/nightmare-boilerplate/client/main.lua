ESX = nil
NIGHTMARE = nil

Citizen.CreateThread(function()
	SetNuiFocus(false)

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while NIGHTMARE == nil do
		TriggerEvent('nightmare-base:getSharedObject', function(obj) NIGHTMARE = obj end)
		Citizen.Wait(0)
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if IsControlJustReleased(0, Keys["E"]) then
			SetNuiFocus(true, true)
			-- Do something
		end
	end
end)

-- Close panel
RegisterNUICallback('quit', function()
	SetNuiFocus(false)
end)
